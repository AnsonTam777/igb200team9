﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlantTimer : MonoBehaviour
{
    public float timeLimit;
    public float timeRemaining;
    private float timeToDisplay;
    private float minutes;
    private float seconds;
    private float money;
    public float moneyProduced;
    public float sellAmount;

    private int startIndex = 2;
    private int endIndex;
    public int plotIndex;
    public int timesPointsGen = 0;

    public TMP_Text timerText;
    public TMP_Text moneyCounter;

    public bool freeSpace = true;

    public string plantName = "";
    public string plantInfo = "";
    public Sprite plantImage;

    public ParticleSystem moneyParticle;

    private ParticleSystem particleSystem;

    private GameObject bowerOfBeautyTeen;
    private GameObject bowerOfBeautyAdult;
    private GameObject redSilkyOakTeen;
    private GameObject redSilkyOakAdult;
    private GameObject swampLilyTeen;
    private GameObject swampLilyAdult;
    private GameObject knottedClubRushTeen;
    private GameObject knottedClubRushAdult;
    private GameObject blueberryLilyTeen;
    private GameObject blueberryLilyAdult;
    private GameObject climbingGuineaFlowerTeen;
    private GameObject climbingGuineaFlowerAdult;

    private GameObject[] bowerOfBeautyStages = new GameObject[2];
    private GameObject[] redSilkyOakStages = new GameObject[2];
    private GameObject[] swampLilyStages = new GameObject[2];
    private GameObject[] knottedClubRushStages = new GameObject[2];
    private GameObject[] blueberryLilyStages = new GameObject[2];
    private GameObject[] climbingGuineaFlowerStages = new GameObject[2];

    private GameObject plantStagesHolder;


    // Start is called before the first frame update
    void Start()
    {
        // Make timer text visible
        timerText.enabled = true;

        // Set and display the timer
        timeRemaining = timeLimit;
        
        // Converting the timer to minutes and seconds
        minutes = Mathf.FloorToInt(timeRemaining / 60);
        seconds = Mathf.FloorToInt(timeRemaining % 60);

        // Formatting the timer
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

        // Initialise all the plant stages prefabs
        plantStagesHolder = GameObject.Find("PlantStages");
        bowerOfBeautyTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().bowerOfBeautyTeen;
        bowerOfBeautyAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().bowerOfBeautyAdult;
        redSilkyOakTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().redSilkyOakTeen;
        redSilkyOakAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().redSilkyOakAdult;
        swampLilyTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().swampLilyTeen;
        swampLilyAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().swampLilyAdult;
        knottedClubRushTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().knottedClubRushTeen;
        knottedClubRushAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().knottedClubRushAdult;
        //blueberryLilyTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().blueberryLilyTeen;
        //blueberryLilyAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().blueberryLilyAdult;
        climbingGuineaFlowerTeen = plantStagesHolder.GetComponent<PlantStagesHolder>().climbingGuineaFlowerTeen;
        climbingGuineaFlowerAdult = plantStagesHolder.GetComponent<PlantStagesHolder>().climbingGuineaFlowerAdult;

        // Organise all the plant stage prefabs into the suitable arrays
        bowerOfBeautyStages[0] = bowerOfBeautyTeen;
        bowerOfBeautyStages[1] = bowerOfBeautyAdult;
        redSilkyOakStages[0] = redSilkyOakTeen;
        redSilkyOakStages[1] = redSilkyOakAdult;
        swampLilyStages[0] = swampLilyTeen;
        swampLilyStages[1] = swampLilyAdult;
        knottedClubRushStages[0] = knottedClubRushTeen;
        knottedClubRushStages[1] = knottedClubRushAdult;
        //blueberryLilyStages[0] = blueberryLilyTeen;
        //blueberryLilyStages[1] = blueberryLilyAdult;
        climbingGuineaFlowerStages[0] = climbingGuineaFlowerTeen;
        climbingGuineaFlowerStages[1] = climbingGuineaFlowerAdult;

        // Determine the beginning sell amount
        sellAmount = 15;
    }

    // Update is called once per frame
    void Update()
    {
        // Generating money
        if (!freeSpace)
        {
            if (timeRemaining > 0)
            {
                // Count down the timer
                timeRemaining -= Time.deltaTime;

                // Convert remaining time to minutes
                minutes = Mathf.FloorToInt(timeRemaining / 60);

                // Conver remaining time to seconds
                seconds = Mathf.FloorToInt(timeRemaining % 60);

                // Change plant timer
                if (timeRemaining >= 0)
                    timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
            }
            else
            {
                // Get the length of characters in the string
                endIndex = moneyCounter.text.Length - startIndex;

                // Get the part of the string that contains the amount of money
                money = float.Parse(moneyCounter.text);
                money += moneyProduced;

                // Add new money to money counter
                moneyCounter.text = (money.ToString());

                // Play the money particle system and play a sound
                PlayMoneyEffect();
                FindObjectOfType<AudioManager>().Play("earning_ecopoints");

                // Reset the timer
                timeRemaining = timeLimit;
                timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

                timesPointsGen += 1;
            }
        }

        // Check if plant can  level up
        if (timesPointsGen == 5)
        {
            if (plantName == "Bower of Beauty")
                LevelPlant(bowerOfBeautyTeen);
            else if (plantName == "Red Silky Oak")
                LevelPlant(redSilkyOakTeen);
            else if (plantName == "Swamp Lily")
                LevelPlant(swampLilyTeen);
            else if (plantName == "Knotted Club Rush")
                LevelPlant(swampLilyTeen);
            /*else if (plantName == "Blueberry Lily")
                LevelPlant(blueberryLilyTeen);*/
            else if (plantName == "Climbing Guinea Flower")
                LevelPlant(climbingGuineaFlowerTeen);
        }
        else if (timesPointsGen == 15)
        {
            if (plantName == "Bower of Beauty")
                LevelPlant(bowerOfBeautyAdult);
            else if (plantName == "Red Silky Oak")
                LevelPlant(redSilkyOakAdult);
            else if (plantName == "Swamp Lily")
                LevelPlant(swampLilyAdult);
            else if (plantName == "Knotted Club Rush")
                LevelPlant(swampLilyAdult);
            /*else if (plantName == "Blueberry Lily")
                LevelPlant(blueberryLilyAdult);*/
            else if (plantName == "Climbing Guinea Flower")
                LevelPlant(climbingGuineaFlowerAdult);
        }
    }

    void PlayMoneyEffect()
    {
        // Play money particle system
        particleSystem = Instantiate(moneyParticle, new Vector3(0, 0, 0), Quaternion.identity);
        particleSystem.transform.parent = this.transform.parent;
        particleSystem.transform.Rotate(-90, 0, 0);
        particleSystem.transform.position = this.transform.position;
        particleSystem.Play();
    }

    void LevelPlant(GameObject newPlant)
    {
        // Instantiate teen prefab version and match it to the previous version
        GameObject upgPlant = Instantiate(newPlant, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        upgPlant.transform.parent = this.transform.parent;
        upgPlant.transform.position = this.transform.position;
        upgPlant.transform.localScale = this.transform.localScale;
        upgPlant.transform.tag = "Plot";
        upgPlant.AddComponent<PlantTimer>();
        upgPlant.GetComponent<PlantTimer>().timerText = this.timerText;
        upgPlant.GetComponent<PlantTimer>().moneyCounter = this.moneyCounter;

        // Fill out the plants properties
        upgPlant.GetComponent<PlantTimer>().plotIndex = this.plotIndex;
        upgPlant.GetComponent<PlantTimer>().freeSpace = this.freeSpace;
        upgPlant.GetComponent<PlantTimer>().timeLimit = this.timeLimit;
        upgPlant.GetComponent<PlantTimer>().moneyProduced = this.moneyProduced;
        upgPlant.GetComponent<PlantTimer>().plantName = this.plantName;
        upgPlant.GetComponent<PlantTimer>().plantInfo = this.plantInfo;
        upgPlant.GetComponent<PlantTimer>().plantImage = this.plantImage;
        upgPlant.GetComponent<PlantTimer>().moneyParticle = this.moneyParticle;
        upgPlant.GetComponent<PlantTimer>().sellAmount = this.sellAmount + 15;
        upgPlant.GetComponent<PlantTimer>().timesPointsGen = this.timesPointsGen + 1;

        upgPlant.GetComponent<PlantTimer>().enabled = true;

        // Remove the previous version of the plant
        Destroy(this.gameObject);
    }
}
