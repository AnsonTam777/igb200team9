﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TriviaQuestions : MonoBehaviour
{
    public TMP_Text plantName;
    public GameObject plantInfoMenu;
    public GameObject triviaMenu;
    public TMP_Text plantIndex;
    public TMP_Text result;

    // List of Questions
    private string[] bowerOfBeautyQues = new string[3];
    private string[] redSilkyOakQues = new string[3];
    private string[] swampLilyQues = new string[3];
    private string[] knottedClubRushQues = new string[3];
    private string[] blueBerryLilyQues = new string[3];
    private string[] climbingGuineaFlowerQues = new string[3];

    // List of answers
    private string[] bowerOfBeauty1Ans = new string[4];
    private string[] bowerOfBeauty2Ans = new string[4];
    private string[] bowerOfBeauty3Ans = new string[4];
    private string[] redSilkyOak1Ans = new string[4];
    private string[] redSilkyOak2Ans = new string[2];
    private string[] redSilkyOak3Ans = new string[4];
    private string[] swampLily1Ans = new string[4];
    private string[] swampLily2Ans = new string[4];
    private string[] swampLily3Ans = new string[4];
    private string[] knottedClubRush1Ans = new string[4];
    private string[] knottedClubRush2Ans = new string[4];
    private string[] knottedClubRush3Ans = new string[4];
    private string[] blueBerryLily1Ans = new string[4];
    private string[] blueBerryLily2Ans = new string[3];
    private string[] blueBerryLily3Ans = new string[4];
    private string[] climbingGuineaFlower1Ans = new string[4];
    private string[] climbingGuineaFlower2Ans = new string[4];
    private string[] climbingGuineaFlower3Ans = new string[4];


    // Start is called before the first frame update
    void Start()
    {
        // Generates the questions and answers
        GenerateQuestions();
        GenerateAnswers();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Generates all the questions for every plant
    private void GenerateQuestions()
    {
        // Generate bower of beauty questions
        bowerOfBeautyQues[0] = "WHen is the flowering time of Bower of Beauty?";
        bowerOfBeautyQues[1] = "Where can you find the Bower of Beauty in Eastern Queensland?";
        bowerOfBeautyQues[2] = "Where are the best light conditions for Bower of Beauty?";

        // Generate red silky oak questions
        redSilkyOakQues[0] = "When are the times that red silky oaks flowers?";
        redSilkyOakQues[1] = "Are the flowers and seed pods toxic?";
        redSilkyOakQues[2] = "What is the colour of red silky oak in other forms?";

        // Generate swamp lily questions
        swampLilyQues[0] = "When are the flowing times of swamp lily?";
        swampLilyQues[1] = "What are the best light conditions for swamp lily?";
        swampLilyQues[2] = "How many white, fragment flowers are usually in a cluster for swamp lily?";

        // Generate knotted club rush questions
        knottedClubRushQues[0] = "What are the suitable soil type(s) for knotted club-rush?";
        knottedClubRushQues[1] = "When are the flowing times of knotted club-rush?";
        knottedClubRushQues[2] = "What are the other states (except QLD) that knotted club-rush are native to?";

        // Generate blueberry lily questions
        blueBerryLilyQues[0] = "What is/are the suitable light conditions for Blueberry Lily?";
        blueBerryLilyQues[1] = "Are the berries produced by Blueberry Lily edible?";
        blueBerryLilyQues[2] = "When is/are the flowering time of Blueberry Lily?";

        // Generate climbing guinea flower questions
        climbingGuineaFlowerQues[0] = "What is/are the best light conditions for Climbing Guinea Flower?";
        climbingGuineaFlowerQues[1] = "When was Climbing Guinea Flower first scribed by German botanist Carl Wildenow who gave it the name Dilenia scandens?";
        climbingGuineaFlowerQues[2] = "When is the flowering time for Climbing Guinea Flower?";
    }

    // Generates all the answers to every question, there can only be a max of 4 answers
    // per question
    private void GenerateAnswers()
    {
        // Answers to bowerOfBeautyQues[0]
        bowerOfBeauty1Ans[0] = "Spring";
        bowerOfBeauty1Ans[1] = "Spring and Summer";
        bowerOfBeauty1Ans[2] = "Autumn and Winter";
        bowerOfBeauty1Ans[3] = "Autumn";

        // Answers to bowerOfBeautyQues[1]
        bowerOfBeauty2Ans[0] = "Tropical and subtropical rainforest";
        bowerOfBeauty2Ans[1] = "Dry forest therein";
        bowerOfBeauty2Ans[2] = "Warm temperate areas";
        bowerOfBeauty2Ans[3] = "All of the above";

        // Answers to bowerOfBeautyQues[2]
        bowerOfBeauty3Ans[0] = "Full sun or part sun";
        bowerOfBeauty3Ans[1] = "Part sun or light shadee";
        bowerOfBeauty3Ans[2] = "Light shade or half shade";
        bowerOfBeauty3Ans[3] = "Any light conditions";

        // Answers to redSilkyQues[0]
        redSilkyOak1Ans[0] = "Most of the year, but mainly in winter and spring";
        redSilkyOak1Ans[1] = "Most of the year, but mainly in summer and autumn";
        redSilkyOak1Ans[2] = "Only in winter";
        redSilkyOak1Ans[3] = "Only in summer";

        // Answers to redSilkyQues[1]
        redSilkyOak2Ans[0] = "Yes";
        redSilkyOak2Ans[1] = "No";

        // Answers to redSilkyQues[2]
        redSilkyOak3Ans[0] = "White forms";
        redSilkyOak3Ans[1] = "Yellow forms";
        redSilkyOak3Ans[2] = "Blue forms";
        redSilkyOak3Ans[3] = "Purplesforms";

        // Answers to swampLilyQues[0]
        swampLily1Ans[0] = "Summer and Autumn";
        swampLily1Ans[1] = "Spring and summer";
        swampLily1Ans[2] = "Autumn and winter";
        swampLily1Ans[3] = "Summer";

        // Answers to swampLilyQues[1]
        swampLily2Ans[0] = "Sunny";
        swampLily2Ans[1] = "Light shade";
        swampLily2Ans[2] = "Half shade";
        swampLily2Ans[3] = "All of the above";

        // Answers to swampLilyQues[2]
        swampLily3Ans[0] = "10 to 100 flowers";
        swampLily3Ans[1] = "10 to 25 flowers";
        swampLily3Ans[2] = "10 to 50 flowers";
        swampLily3Ans[3] = "20 to 50 flowers";

        // Answers to knottedClubRushQues[0]
        knottedClubRush1Ans[0] = "Gravel";
        knottedClubRush1Ans[1] = "Sand";
        knottedClubRush1Ans[2] = "Loam";
        knottedClubRush1Ans[3] = "All of the above";

        // Answers to knottedClubRushQues[1]
        knottedClubRush2Ans[0] = "Most of the year";
        knottedClubRush2Ans[1] = "Spring and summer";
        knottedClubRush2Ans[2] = "Autumn and winter";
        knottedClubRush2Ans[3] = "Summer";

        // Answers to knottedClubRushQues[2]
        knottedClubRush3Ans[0] = "All Australia states";
        knottedClubRush3Ans[1] = "NSW and Victoria";
        knottedClubRush3Ans[2] = "NT and WA";
        knottedClubRush3Ans[3] = "All Australia states except Victoria";

        // Answers to blueBerryLilyQues[0]
        blueBerryLily1Ans[0] = "Full-sun";
        blueBerryLily1Ans[1] = "Part-shade";
        blueBerryLily1Ans[2] = "Full-shade";
        blueBerryLily1Ans[3] = "All of the above";

        // Answers to blueBerryLilyQues[1]
        blueBerryLily2Ans[0] = "Yes";
        blueBerryLily2Ans[1] = "Yes if after process";
        blueBerryLily2Ans[2] = "No";

        // Answers to blueBerryLilyQues[2]
        blueBerryLily3Ans[0] = "Spring and Summer";
        blueBerryLily3Ans[1] = "Spring";
        blueBerryLily3Ans[2] = "Summer";
        blueBerryLily3Ans[3] = "Summer and Autumn";

        // Answers to climbingGuineaFlowerQues[0]
        climbingGuineaFlower1Ans[0] = "Full-sun";
        climbingGuineaFlower1Ans[1] = "Part-shade";
        climbingGuineaFlower1Ans[2] = "Full-shade";
        climbingGuineaFlower1Ans[3] = "All of the above";

        // Answers to climbingGuineaFlowerQues[1]
        climbingGuineaFlower2Ans[0] = "1790s years";
        climbingGuineaFlower2Ans[1] = "1790s years";
        climbingGuineaFlower2Ans[2] = "1800s years";
        climbingGuineaFlower2Ans[3] = "1810s years";

        // Answers to climbingGuineaFlowerQues[2]
        climbingGuineaFlower3Ans[0] = "Spring and Summer";
        climbingGuineaFlower3Ans[1] = "Spring";
        climbingGuineaFlower3Ans[2] = "Summer";
        climbingGuineaFlower3Ans[3] = "Summer and Autumn";
    }

    public void pickPlant()
    {
        // Determine what plant to load questions for
        if (plantName.text == "Bower of Beauty")
        {
            bowerOfBeauty();
        }

        else if (plantName.text == "Red Silky Oak")
        {
            redSilkyOak();
        }

        else if (plantName.text == "Swamp Lily")
        {
            swampLily();
        }

        else if (plantName.text == "Knotted Club Rush")
        {
            knottedClubRush();
        }
    }

    private void OpenCloseMenu(bool activeState)
    {
        // Open/Close Plant Info Menu
        plantInfoMenu.SetActive(activeState);

        // Open/Close trivia menu
        triviaMenu.SetActive(!activeState);
        result.text = "";
        Debug.Log(triviaMenu.activeSelf);
    }

    // Checks the answer of the first option when clicked
    public void CheckAnswer1()
    {
        if (result.text != "CORRECT") //&& result.text != "WRONG")
        {
            string answer = GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text;
            if (plantName.text == "Bower of Beauty")
            {
                if (answer == bowerOfBeauty2Ans[0])
                    result.text = "CORRECT";
                else if (answer == bowerOfBeauty3Ans[0])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Red Silky Oak")
            {
                if (answer == redSilkyOak1Ans[0])
                    result.text = "CORRECT";
                else if (answer == redSilkyOak2Ans[0])
                    result.text = "CORRECT";
                else if (answer == redSilkyOak3Ans[0])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Swamp Lily")
            {
                if (answer == swampLily1Ans[0])
                    result.text = "CORRECT";
                else if (answer == swampLily3Ans[0])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Knotted club-rush")
            {
                if (answer == knottedClubRush2Ans[0])
                    result.text = "CORRECT";
                else if (answer == knottedClubRush3Ans[0])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Blueberry Lily")
            {
                if (answer == blueBerryLily3Ans[0])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Climbing Guinea Flower")
            {
                if (answer == climbingGuineaFlower1Ans[0])
                    result.text = "CORRECT";
                else if (answer == climbingGuineaFlower3Ans[0])
                    result.text = "CORRECT";
            }

            if (result.text != "CORRECT")
                result.text = "WRONG";
            else
                ApplyTimeBonus();
        }
    }

    // Checks the answer of the second option when clicked
    public void CheckAnswer2()
    {
        if (result.text != "CORRECT" && result.text != "WRONG")
        {
            string answer = GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text;

            if (plantName.text == "Bower of Beauty")
            {
                if (answer == bowerOfBeauty1Ans[1])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Blueberry Lily")
            {
                if (answer == blueBerryLily2Ans[1])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Climbing Guinea Flower")
            {
                if (answer == climbingGuineaFlower2Ans[1])
                    result.text = "CORRECT";
            }
            if (result.text != "CORRECT")
                result.text = "WRONG";
            else
                ApplyTimeBonus();
        }
    }

    // Checks the answer of the third option when clicked
    public void CheckAnswer3()
    {
        if (result.text != "CORRECT" && result.text != "WRONG")
        {
            string answer = GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text;
            

            if (result.text != "CORRECT")
                result.text = "WRONG";
            else
                ApplyTimeBonus();
        }
    }

    // Checks the answer of the fourth option when clicked
    public void CheckAnswer4()
    {
        if (result.text != "CORRECT" && result.text != "WRONG")
        {
            string answer = GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text;
            
            if (plantName.text == "Swamp Lily")
            {
                if (answer == swampLily2Ans[3])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Knotted club-rush")
            {
                if (answer == knottedClubRush1Ans[3])
                    result.text = "CORRECT";
            }
            else if (plantName.text == "Blueberry Lily")
            {
                if (answer == blueBerryLily1Ans[3])
                    result.text = "CORRECT";
            }

            if (result.text != "CORRECT")
                result.text = "WRONG";
            else
                ApplyTimeBonus();
        }
    }

    private void ApplyTimeBonus()
    {
        // Find all gameobjects with the tag plot
        GameObject[] allPlotsPurchased = GameObject.FindGameObjectsWithTag("Plot");

        // Reduce the plants time by half
        int index = int.Parse(plantIndex.text);
        float timeRemaining = allPlotsPurchased[index].GetComponent<PlantTimer>().timeRemaining;
        timeRemaining = timeRemaining - (timeRemaining / 2);
        allPlotsPurchased[index].GetComponent<PlantTimer>().timeRemaining = timeRemaining;
    }

    public void GoBack()
    {
        OpenCloseMenu(true);
    }

    private void bowerOfBeauty()
    {
        OpenCloseMenu(false);

        // Pick a question randomly
        int index = Random.Range(0, 2);
        string question = bowerOfBeautyQues[index];
        GameObject.Find("Question").GetComponent<TMP_Text>().text = question;

        // Place the answers on the buttons and enable the answer buttons
        switch (index)
        {
            case 0:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = bowerOfBeauty1Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = bowerOfBeauty1Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = bowerOfBeauty1Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = bowerOfBeauty1Ans[3];
                break;
            case 1:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = bowerOfBeauty2Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = bowerOfBeauty2Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = bowerOfBeauty2Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = bowerOfBeauty2Ans[3];
                break;
            case 2:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = bowerOfBeauty3Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = bowerOfBeauty3Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = bowerOfBeauty3Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = bowerOfBeauty3Ans[3];
                break;
            default:
                break;
        }

    }

    private void redSilkyOak()
    {
        OpenCloseMenu(false);

        // Pick a question randomly
        int index = Random.Range(0, 2);
        string question = redSilkyOakQues[index];
        GameObject.Find("Question").GetComponent<TMP_Text>().text = question;

        // Place the answers on the buttons and enable the answer buttons
        switch (index)
        {
            case 0:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = redSilkyOak1Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = redSilkyOak1Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = redSilkyOak1Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = redSilkyOak1Ans[3];
                break;
            case 1:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = redSilkyOak2Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = redSilkyOak2Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = "";
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = "";
                break;
            case 2:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = redSilkyOak3Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = redSilkyOak3Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = redSilkyOak3Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = redSilkyOak3Ans[3];
                break;
            default:
                break;
        }


    }

    private void swampLily()
    {
        OpenCloseMenu(false);

        // Pick a question randomly
        int index = Random.Range(0, 2);
        string question = swampLilyQues[index];
        GameObject.Find("Question").GetComponent<TMP_Text>().text = question;

        // Place the answers on the buttons and enable the answer buttons
        switch (index)
        {
            case 0:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = swampLily1Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = swampLily1Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = swampLily1Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = swampLily1Ans[3];
                break;
            case 1:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = swampLily2Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = swampLily2Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = swampLily2Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = swampLily2Ans[3];
                break;
            case 2:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = swampLily3Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = swampLily3Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = swampLily3Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = swampLily3Ans[3];
                break;
            default:
                break;
        }
    }

    private void knottedClubRush()
    {
        OpenCloseMenu(false);

        // Pick a question randomly
        int index = Random.Range(0, 2);
        string question = knottedClubRushQues[index];
        GameObject.Find("Question").GetComponent<TMP_Text>().text = question;

        switch (index)
        {
            case 0:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = knottedClubRush1Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = knottedClubRush1Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = knottedClubRush1Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = knottedClubRush1Ans[3];
                break;
            case 1:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = knottedClubRush2Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = knottedClubRush2Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = knottedClubRush2Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = knottedClubRush2Ans[3];
                break;
            case 2:
                GameObject.Find("Answer1Text").GetComponent<TMP_Text>().text = knottedClubRush3Ans[0];
                GameObject.Find("Answer2Text").GetComponent<TMP_Text>().text = knottedClubRush3Ans[1];
                GameObject.Find("Answer3Text").GetComponent<TMP_Text>().text = knottedClubRush3Ans[2];
                GameObject.Find("Answer4Text").GetComponent<TMP_Text>().text = knottedClubRush3Ans[3];
                break;
            default:
                break;
        }
    }
}
