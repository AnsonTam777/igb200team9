﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuManager : MonoBehaviour
{
    public Image shopMenu;
    public Image starRatingBar;
    public Image level2EcoIcon;
    public GameObject PlantsMenu;
    public GameObject plot1;
    public GameObject plot2;
    public GameObject plot3;
    public GameObject plot4;
    public GameObject plot5;
    public GameObject plot6;
    public GameObject purchasablePlots;
    public GameObject purchasablePlants;
    public GameObject purchasableDecors;
    public GameObject purchasableAnimals;
    public GameObject decor1;
    public GameObject decor2;
    public GameObject decor3;
    public GameObject decor4;
    public GameObject animal1;
    public GameObject animal2;
    public GameObject animal3;
    public GameObject animal4;

    private GameObject[] allPlotsPurchased;
    private GameObject[] allAnimalsPurchased;

    private string[,] AniRequirements = new string[6, 2] { {"Bower of Beauty", "Bee"}, {"Red Sily Oak", "Bee"}, {"Swamp Lily", "Butterfly"}, {"Blueberry Lily", "Parrot"}, {"Knotted Club-rush", "Bird"}, {"Climbing Guinea Flower", "Lizard"}};
    

    public Sprite bowerOfBeautyImage;
    public Sprite redSilkyOakImage;
    public Sprite swampLilyImage;
    public Sprite knottedClubRushImage;
    public Sprite blueberryLilyImage;
    public Sprite climbingGuineaFlowerImage;
    public Sprite bowerOfBeautyModel;
    public Sprite redSilkyOakModel;
    public Sprite swampLilyModel;
    public Sprite knottedClubRushModel;
    public Sprite blueberryLilyModel;
    public Sprite climbingGuineaFlowerModel;

    public GameObject bowerOfBeautyBaby;
    public GameObject redSilkyOakBaby;
    public GameObject swampLilyBaby;
    public GameObject knottedClubRushBaby;
    public GameObject blueberryLilyBaby;
    public GameObject climbingGuineaFlowerBaby;
    public GameObject simplePot;

    public bool level2Unlocked = false;
    public bool level2StarsFull = false;
    public bool level3Unlocked = false;
    public bool level3StarsFull = false;
    private bool plantPlaced = false;

    private static float moneyProduced;
    private float timer;
    private string plantName;
    private string plantInfo;
    public TMP_Text moneyCounter;
    private string money;
    private int startIndex;
    private int endIndex;
    private float currentMoney;
    private float starRating;
    private float starItems;
    public Image starMeterFilling;
    public int currentStarRating;
    public GameObject level2Price;
    public TMP_Text level2Text;
    public TMP_Text level3Text;

    public ParticleSystem particleEffect;

    // Start is called before the first frame update
    void Start()
    {
        // Determine the starRating based on the level and amount of items
        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "Level1")
            starItems = 7f; // 3 pots + 4 decors
        else if (scene.name == "Level2")
            starItems = 10f; // 6 pots + 4 decors

        // Determines how much the star rating fills with each purchase
        starRating = 1f / starItems;
        
        moneyCounter.text = GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().currentMoney.ToString();

        if (scene.name == "Level1")
            starMeterFilling.fillAmount = GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().level1StarsFloat;
        else if (scene.name == "Level2")
            starMeterFilling.fillAmount = GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().level2StarsFloat;
    }

    // Update is called once per frame
    void Update()
    {
        money = moneyCounter.text;
        currentMoney = float.Parse(money);

        level2Unlocked = GameObject.Find("GameScene").GetComponent<GameScene>().
            level2Unlocked;
        level3Unlocked = GameObject.Find("GameScene").GetComponent<GameScene>().
            level3Unlocked;

        // Determine the current amount of stars
        if (starMeterFilling.fillAmount == starRating * 5)
            currentStarRating = 5;
        else if (starMeterFilling.fillAmount >= starRating * 4)
            currentStarRating = 4;
        else if (starMeterFilling.fillAmount >= starRating * 3)
            currentStarRating = 3;
        else if (starMeterFilling.fillAmount >= starRating * 2)
            currentStarRating = 2;
        else if (starMeterFilling.fillAmount >= starRating)
            currentStarRating = 1;
        else
            currentStarRating = 0;
    }

    // Shop Menu Tabs
    public void OnShopPlotClick()
    {
        purchasablePlots.SetActive(true);
        purchasablePlants.SetActive(false);
        purchasableDecors.SetActive(false);
        purchasableAnimals.SetActive(false);
    }

    public void OnShopPlantClick()
    {
        purchasablePlots.SetActive(false);
        purchasablePlants.SetActive(true);
        purchasableDecors.SetActive(false);
        purchasableAnimals.SetActive(false);

        // Unlock new plants in the shop menu
        if (level2Unlocked)
        {
            // Change lock image to image of new plant and add price of new plant
            GameObject.Find("Plant3Image").transform.GetComponent<Image>().sprite = swampLilyImage;
            GameObject.Find("Plant3Button").transform.GetComponentInChildren<TMP_Text>().text = "$15";

            GameObject.Find("Plant4Image").transform.GetComponent<Image>().sprite = knottedClubRushImage;
            GameObject.Find("Plant4Button").transform.GetComponentInChildren<TMP_Text>().text = "$25";
        }

        if (level3Unlocked)
        {
            GameObject.Find("Plant5Image").transform.GetComponent<Image>().sprite = blueberryLilyImage;
            GameObject.Find("Plant5Button").transform.GetComponentInChildren<TMP_Text>().text = "$35";

            GameObject.Find("Plant6Image").transform.GetComponent<Image>().sprite = climbingGuineaFlowerImage;
            GameObject.Find("Plant6Button").transform.GetComponentInChildren<TMP_Text>().text = "$45";
        }
    }

    public void OnShopDecorClick()
    {
        purchasablePlots.SetActive(false);
        purchasablePlants.SetActive(false);
        purchasableDecors.SetActive(true);
        purchasableAnimals.SetActive(false);
    }

    public void OnShopAnimalClick()
    {
        purchasablePlots.SetActive(false);
        purchasablePlants.SetActive(false);
        purchasableDecors.SetActive(false);
        purchasableAnimals.SetActive(true);
    }

    // Shop Menu Purchases
    public void OnShopPlot1()
    {
        if (plot1.activeSelf == false)
        {
            plot1.SetActive(true);

            GameObject potButton = GameObject.Find("Pot1Button");
            TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
            price.text = "Bought";

            starRatingBar.fillAmount += starRating;
        }
    }

    public void OnShopPlot2()
    {
        if (plot2.activeSelf == false)
        {
            if (currentMoney >= 5f)
            {
                plot2.SetActive(true);

                GameObject potButton = GameObject.Find("Pot2Button");
                TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 5f;
                moneyCounter.text = currentMoney.ToString();
                starRatingBar.fillAmount += starRating;
            }
        }
    }

    public void OnShopPlot3()
    {
        if (plot3.activeSelf == false)
        {
            if (currentMoney >= 15)
            {
                plot3.SetActive(true);
                GameObject potButton = GameObject.Find("Pot3Button");
                TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 15f;
                moneyCounter.text = currentMoney.ToString();
                starRatingBar.fillAmount += starRating;
            }
        }
    }

    public void OnShopPlot4()
    {
        if (plot4.activeSelf == false)
        {
            if (currentMoney >= 25)
            {
                plot4.SetActive(true);
                GameObject potButton = GameObject.Find("Pot4Button");
                TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 25f;
                moneyCounter.text = currentMoney.ToString();
                starRatingBar.fillAmount += starRating;
            }
        }
    }

    public void OnShopPlot5()
    {
        if (plot5.activeSelf == false)
        {
            if (currentMoney >= 35)
            {
                plot5.SetActive(true);
                GameObject potButton = GameObject.Find("Pot5Button");
                TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 35f;
                moneyCounter.text = currentMoney.ToString();
                starRatingBar.fillAmount += starRating;
            }
        }
    }

    public void OnShopPlot6()
    {
        if (plot6.activeSelf == false)
        {
            if (currentMoney >= 45)
            {
                plot6.SetActive(true);
                GameObject potButton = GameObject.Find("Pot6Button");
                TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 45f;
                moneyCounter.text = currentMoney.ToString();
                starRatingBar.fillAmount += starRating;
            }
        }
    }

    public void OnShopPlant1()
    {
        if (currentMoney >= 5f)
        {
            // Set the timer and money produced
            timer = 7f;
            moneyProduced = 10f;
            plantName = "Bower of Beauty";
            plantInfo = "Pandorea jasminoides, variously known as Bower of Beauty, Bower Vine or Bower Climber. " +
                "Glossy dark green leaves are mostly opposite or in whorls of three and 12 to 20 cm long.Each " +
                "leaf consists of 4 to 7 leaflets with lanceolate to ovate leaves. Flowers are white or pale " +
                "pink, trumpet shaped and up to six centimetres long.Blooms are carried in clusters from spring " +
                "to summer.";

            // Place the plant into a available plot
            plantPlaced = false;
            plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, bowerOfBeautyModel);

            if (plantPlaced)
            {
                currentMoney -= 5f;
                moneyCounter.text = (currentMoney.ToString());
            }
        }
    }

    public void OnShopPlant2()
    {
        if (currentMoney >= 10f)
        {
            timer = 10f;
            moneyProduced = 20f;
            plantName = "Red Silky Oak";
            plantInfo = "Bushy to spindly erect shrubs or slender trees, 2-10m high, or rarely a prostate " +
                "to sprawling shrub. Leaves 8-30 cm long, pinnatipartite (rarely the odd leaf entire); " +
                "lobes 4-12, narrowly elliptic to linear, 5-18 cm long, 5-15 mm wide, not pungent; margins shortly " +
                "recurved or revolute; lower surface usually mostly exposed, sub sericeous to sub villous.";

            plantPlaced = false;
            plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, redSilkyOakModel);

            if (plantPlaced)
            {
                currentMoney -= 10f;
                moneyCounter.text = (currentMoney.ToString());
            }
        }
    }

    public void OnShopPlant3()
    {
        if (currentMoney >= 15f)
        {
            if (level2Unlocked)
            {
                timer = 15f;
                moneyProduced = 30f;
                plantName = "Swamp Lily";
                plantInfo = "Crinum pedunculatum is strictly described as a large bulbous perennial herb, " +
                    "which may reach 2 (or even 3) metres high by a 2-3 metre spread. The leaves are strappy " +
                    "and up to 2m long by 15cm wide and thickened, with blunt points. The flowers, occurring " +
                    "from November to March, are white and about 10cm across in clusters of 10-25. They are " +
                    "pleasantly perfumed. They are followed by rounded, beaked seed capsules 2 to 5cm across.";

                plantPlaced = false;
                plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, swampLilyModel);

                if (plantPlaced)
                {
                    currentMoney -= 15f;
                    moneyCounter.text = (currentMoney.ToString());
                }
            }
        }
    }

    public void OnShopPlant4()
    {
        if (currentMoney >= 25f)
        {
            if (level2Unlocked)
            {
                timer = 25f;
                moneyProduced = 50f;
                plantName = "Knotted Club Rush";
                plantInfo = "Flower-head, dense and globular or hemispherical in shape, 7-20 mm diameter " +
                    "with numerous spikelets, subtended by a rigid and sharply pointed bract. Fruit a " +
                    "smooth, glossy, dark brown to black nut of irregular shape and 1 mm diameter.";

                plantPlaced = false;
                plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, swampLilyModel);

                if (plantPlaced)
                {
                    currentMoney -= 25f;
                    moneyCounter.text = (currentMoney.ToString());
                }
            }
        }
    }

    public void OnShopPlant5()
    {
        if (currentMoney >= 35f)
        {
            if (level3Unlocked)
            {
                timer = 35f;
                moneyProduced = 70f;
                plantName = "Blueberry Lily";
                plantInfo = "A hardy perennial native herb 0.5 to 1.5m tall, cultivated as a garden ornamental " +
                    "for its attractive foliage and striking blue/purple berries. Flowers can range in colour from " +
                    "pale blue through to deep blue/purple, are roughly 1 to 2cm wide with 6 petals with 6 exerted " +
                    "yellow to brown stamens. The flower spike arises from the plant base with flowers in terminal " +
                    "clusters on a branched central stalk. The stiff leaves are strap-like, long and narrow " +
                    "sometimes with a waxy bloom on the under surface.";

                plantPlaced = false;
                plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, swampLilyModel);

                if (plantPlaced)
                {
                    currentMoney -= 35f;
                    moneyCounter.text = (currentMoney.ToString());
                }
            }
        }
    }

    public void OnShopPlant6()
    {
        if (currentMoney >= 45f)
        {
            if (level3Unlocked)
            {
                timer = 45f;
                moneyProduced = 90f;
                plantName = "Climbing Guinea Flower";
                plantInfo = "Hibbertia scandens grow naturally along the coastal fringe of South-eastern New South Wales" +
                    " to north-east Queensland from coastal dunes to forested areas. It is a fairly vigorous climber or " +
                    "scrambler, growing to 2 to 5 metres long.  The large golden yellow flowers, 5-7cm across that appear " +
                    "in spring and summer, complement the glossy green leaves very well to make an attractive climber";

                plantPlaced = false;
                plantPlaced = purchasePlant(timer, moneyProduced, plantName, plantInfo, swampLilyModel);

                if (plantPlaced)
                {
                    currentMoney -= 45f;
                    moneyCounter.text = (currentMoney.ToString());
                }
            }
        }
    }

    public void OnShopDecor1()
    {
        if (currentMoney >= 15)
        {
            if (decor1.activeSelf == false)
            {
                GameObject decorButton = GameObject.Find("Decor1Button");
                TMP_Text price = decorButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 15f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            decor1.SetActive(true);
        }
    }

    public void OnShopDecor2()
    {
        if (currentMoney >= 30)
        {
            if (decor2.activeSelf == false)
            {
                GameObject decorButton = GameObject.Find("Decor2Button");
                TMP_Text price = decorButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 30f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            decor2.SetActive(true);
        }
    }

    public void OnShopDecor3()
    {
        if (currentMoney >= 50)
        {
            if (decor3.activeSelf == false)
            {
                GameObject decorButton = GameObject.Find("Decor3Button");
                TMP_Text price = decorButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 50f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            decor3.SetActive(true);
        }
    }

    public void OnShopDecor4()
    {
        if (currentMoney >= 65)
        {
            if (decor4.activeSelf == false)
            {
                GameObject decorButton = GameObject.Find("Decor4Button");
                TMP_Text price = decorButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 65f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            decor4.SetActive(true);
        }
    }

    public void OnShopAni1()
    {
        if (currentMoney >= 15)
        {
            if (animal1.activeSelf == false)
            {
                GameObject animalButton = GameObject.Find("Animal1Button");
                TMP_Text price = animalButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 15f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            animal1.SetActive(true);
        }
    }

    public void OnShopAni2()
    {
        if (currentMoney >= 30)
        {
            if (animal2.activeSelf == false)
            {
                GameObject animalButton = GameObject.Find("Animal2Button");
                TMP_Text price = animalButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 30f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            animal2.SetActive(true);
        }
    }

    public void OnShopAni3()
    {
        if (currentMoney >= 50)
        {
            if (animal3.activeSelf == false)
            {
                GameObject animalButton = GameObject.Find("Animal3Button");
                TMP_Text price = animalButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 50f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            animal3.SetActive(true);
        }
    }

    public void OnShopAni4()
    {
        if (currentMoney >= 65)
        {
            if (animal4.activeSelf == false)
            {
                GameObject animalButton = GameObject.Find("Animal4Button");
                TMP_Text price = animalButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
                price.text = "Bought";

                currentMoney -= 65f;
                // Add new money to money counter
                moneyCounter.text = (currentMoney.ToString());
                starRatingBar.fillAmount += starRating;
            }
            animal4.SetActive(true);
        }
    }

    public bool purchasePlant(float timer, float moneyProduced, string plantName, string plantInfo, Sprite plantImage)
    {
        GameObject emptyPlot;
        bool foundPlot = false;

        // Find all gameobjects with the tag plot
        allPlotsPurchased = GameObject.FindGameObjectsWithTag("Plot");

        // Check all pots until it finds one thats free
        for (int i = 0; i < allPlotsPurchased.Length; i++)
        {
            if (allPlotsPurchased[i].GetComponent<PlantTimer>().freeSpace == true)
            {
                // Set the timer and money produced based on plant type
                // Determine the plant that replaces the empty pot
                if (plantName == "Bower of Beauty")
                    emptyPlot = Instantiate(bowerOfBeautyBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                else if (plantName == "Red Silky Oak")
                    emptyPlot = Instantiate(redSilkyOakBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                else if (plantName == "Swamp Lily")
                    emptyPlot = Instantiate(swampLilyBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                else if (plantName == "Knotted Club Rush")
                    emptyPlot = Instantiate(knottedClubRushBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
               /* else if (plantName == "Blueberry Lily")
                    emptyPlot = Instantiate(blueberryLilyBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;*/
                else if (plantName == "Climbing Guinea Flower")
                    emptyPlot = Instantiate(climbingGuineaFlowerBaby, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                else
                    emptyPlot = Instantiate(simplePot, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;

                // Set the new plant to where the empty plot was
                emptyPlot.transform.parent = GameObject.Find("PlantPots").transform;
                emptyPlot.transform.position = allPlotsPurchased[i].transform.position;
                emptyPlot.transform.localScale = allPlotsPurchased[i].transform.localScale;
                emptyPlot.transform.tag = "Plot";
                emptyPlot.AddComponent<PlantTimer>();
                emptyPlot.GetComponent<PlantTimer>().timerText = allPlotsPurchased[i].GetComponent<PlantTimer>().timerText;
                emptyPlot.GetComponent<PlantTimer>().moneyCounter = allPlotsPurchased[i].GetComponent<PlantTimer>().moneyCounter;
                Destroy(allPlotsPurchased[i]);

                // Fill out the plants properties
                emptyPlot.GetComponent<PlantTimer>().plotIndex = i; Debug.Log("index = " + i);
                emptyPlot.GetComponent<PlantTimer>().freeSpace = false;
                emptyPlot.GetComponent<PlantTimer>().timeLimit = timer;
                emptyPlot.GetComponent<PlantTimer>().moneyProduced = moneyProduced;
                emptyPlot.GetComponent<PlantTimer>().plantName = plantName;
                emptyPlot.GetComponent<PlantTimer>().plantInfo = plantInfo;
                emptyPlot.GetComponent<PlantTimer>().plantImage = plantImage;
                emptyPlot.GetComponent<PlantTimer>().moneyParticle = particleEffect;

                // Start the timers for this plot
                emptyPlot.GetComponent<PlantTimer>().enabled = true;

                // Close for loop
                foundPlot = true;
                Debug.Log(foundPlot);
                i = allPlotsPurchased.Length;
            }
            else
                foundPlot = false;
        }

        return foundPlot;
    }

    public bool purchaseAnimal(bool hasPlant, string animalName, string animalInfo, Sprite animalImage)
    {
        GameObject emptyPlot;
        bool foundAnimal = false;

        // Find all gameobjects with the tag plot
        allPlotsPurchased = GameObject.FindGameObjectsWithTag("Plot");

        // Check all pots until it finds one thats free
        for (int i = 0; i < allPlotsPurchased.Length; i++)
        {
            for (int j = 0; j < AniRequirements.Length; j++)
            {

                if (allPlotsPurchased[i].GetComponent<PlantTimer>().plantName == AniRequirements[j, 0] && animalName == AniRequirements[j, 1])
                {
                    /*
                    // Set the timer and money produced based on plant type
                    emptyPlot = allPlotsPurchased[i];
                    emptyPlot.GetComponent<PlantTimer>().plotIndex = i;
                    emptyPlot.GetComponent<PlantTimer>().freeSpace = false;
                    emptyPlot.GetComponent<PlantTimer>().timeLimit = timer;
                    emptyPlot.GetComponent<PlantTimer>().moneyProduced = moneyProduced;
                    emptyPlot.GetComponent<PlantTimer>().plantName = plantName;
                    emptyPlot.GetComponent<PlantTimer>().plantInfo = plantInfo;
                    emptyPlot.GetComponent<PlantTimer>().plantImage = plantImage;

                    // Start the timers for this plot
                    emptyPlot.GetComponent<PlantTimer>().enabled = true; */
                    

                    // Close for loop
                    foundAnimal = true;
                    Debug.Log("Animal: " + foundAnimal);
                    i = allPlotsPurchased.Length;
                }
                else
                    foundAnimal = false;
            }
        }

        return foundAnimal;
    }

    
    public void OnSellClick()
    {
        // Find all gameobjects with the tag plot
        allPlotsPurchased = GameObject.FindGameObjectsWithTag("Plot");
        
        GameObject sellButton = GameObject.Find("Sell");
        TMP_Text sellText = sellButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
        sellText.text = "Sold";

        if (allPlotsPurchased != null)
        {
            Destroy(allPlotsPurchased[GameScene.plantIndex].transform.gameObject);
            string buttonStr = "";
            /*
            if (GameScene.plantIndex == 0)
            {   
                plot1.SetActive(false);
                buttonStr = "Pot1Button";
            }
            else if (GameScene.plantIndex == 1)
            {   
                plot2.SetActive(false);
                buttonStr = "Pot2Button";
            }
            else if (GameScene.plantIndex == 2)
            {   
                plot3.SetActive(false);
                buttonStr = "Pot3Button";
            }

            GameObject potButton = GameObject.Find(buttonStr);
            TMP_Text price = potButton.transform.GetChild(0).GetComponentInChildren<TMP_Text>();
            price.text = "Reset";
            */
            var test = new List<GameObject>(allPlotsPurchased);
            test.RemoveAt(GameScene.plantIndex);
            allPlotsPurchased = test.ToArray();
            currentMoney += GameScene.plantIndex + 1;
            // Add new money to money counter
            moneyCounter.text = (currentMoney.ToString());
        }

        
    }

    public void OnLevel1Click()
    {
        GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().currentMoney = currentMoney;
        int totalStars = GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().determineStars();
        SceneManager.LoadScene("Level1");
    }

    public void OnLevel2Click()
    {
        int totalStars = GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().determineStars();

        if (level2StarsFull)
        {
            if (!level2Unlocked)
            {
                if (currentMoney >= 50f)
                {
                    currentMoney -= 50f;
                    moneyCounter.text = (currentMoney.ToString());
                    level2Unlocked = true;
                    GameObject.Find("GameScene").GetComponent<GameScene>().level2Unlocked = true;
                    GameObject.Find("Level2Text").GetComponent<TMP_Text>().text = "Bought";
                    level2EcoIcon.enabled = false;
                }
            }

            else
            {
                GameObject.Find("CarriedOver").GetComponent<CarriedOverValues>().currentMoney = currentMoney;
                SceneManager.LoadScene("Level2");
            }
        }
        else
        {
            if (totalStars >= 2)
            {
                level2StarsFull = true;
                GameObject.Find("Level2Star").SetActive(false);
                level2Price.SetActive(true);
            }
        }
    }
}
