﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class CarriedOverValues : MonoBehaviour
{
    // This script acts as a holder for any values that need to be carried over multiple levels
    public float currentMoney;
    public int currentStars;
    public int level1Stars;
    public float level1StarsFloat;
    public int level2Stars;
    public float level2StarsFloat;
    public int level3Stars;

    public TMP_Text moneyCounter;

    // Start is called before the first frame update
    void Start()
    {
        currentStars = 0;

        if (moneyCounter == null)
            moneyCounter = GameObject.Find("MoneyCountertext").GetComponent<TMP_Text>();

        // Determine the starting money
        string money = moneyCounter.text;
        currentMoney = float.Parse(money);
    }

    // Update is called once per frame
    void Update()
    {
        if (moneyCounter == null)
            moneyCounter = GameObject.Find("MoneyCountertext").GetComponent<TMP_Text>();
    }

    public int determineStars()
    {
        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "Level1")
        {
            this.level1Stars = GameObject.Find("Menus").GetComponent<MenuManager>().currentStarRating;
            this.level1StarsFloat = GameObject.Find("Menus").GetComponent<MenuManager>().starMeterFilling.fillAmount;
        }
        else if (scene.name == "Level2")
        {
            this.level2Stars = GameObject.Find("Menus").GetComponent<MenuManager>().currentStarRating;
            this.level2StarsFloat = GameObject.Find("Menus").GetComponent<MenuManager>().starMeterFilling.fillAmount;
        }
        else
        {
            this.level3Stars = GameObject.Find("Menus").GetComponent<MenuManager>().currentStarRating;
        }

        this.currentStars = level1Stars + level2Stars + level3Stars;

        return this.currentStars;
    }
}
