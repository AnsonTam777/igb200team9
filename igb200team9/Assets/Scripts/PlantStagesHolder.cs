﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantStagesHolder : MonoBehaviour
{
    // All plant stages
    public GameObject bowerOfBeautyTeen;
    public GameObject bowerOfBeautyAdult;
    public GameObject redSilkyOakTeen;
    public GameObject redSilkyOakAdult;
    public GameObject swampLilyTeen;
    public GameObject swampLilyAdult;
    public GameObject knottedClubRushTeen;
    public GameObject knottedClubRushAdult;
    public GameObject blueberryLilyTeen;
    public GameObject blueberryLilyAdult;
    public GameObject climbingGuineaFlowerTeen;
    public GameObject climbingGuineaFlowerAdult;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
