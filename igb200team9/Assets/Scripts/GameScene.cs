﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameScene : MonoBehaviour
{
    private CanvasGroup fadeGroup;
    private float fadeInSpeed = 0.33f;
    public Image shopMenu;
    public GameObject LevelMenu;
    public GameObject PlantsMenu;
    public Image AnimalsMenu;
    public Image AnimalInfoMenu;

    public GameObject menuTabs;
    public GameObject plotButton;
    public GameObject plantInfoItems;

    public TMP_Text plantTitle;
    public TMP_Text plantInfo;
    public TMP_Text potIndex;

    public Image plantImage;

    private GameObject[] allPlots;
    public static int plantIndex;

    public bool level2Unlocked = false;
    public bool level3Unlocked = false;

    private void Start()
    {
        //Grab the only CanvasGroup in the scene
        fadeGroup = FindObjectOfType<CanvasGroup>();

        //Start with a white scene;
        fadeGroup.alpha = 1;
    }

    private void Update()
    {
        //Fade-in 
        fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;
        allPlots = GameObject.FindGameObjectsWithTag("Plot");

        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();
    }

    //In-game UI Buttons
    public void OnShopClick()
    {
        shopMenu.enabled = true;
        menuTabs.SetActive(true);
        plotButton.SetActive(true);
    }

    public void OnLevelClick()
    {
        LevelMenu.SetActive(true);
        LevelMenu.transform.Find("LevelSelection").gameObject.SetActive(true);
    }

    public void OnPlantsClick()
    {
        plantIndex = 0;
        if (allPlots != null)
        {
            plantTitle.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantName;
            plantInfo.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantInfo;
            plantImage.sprite = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantImage;
            potIndex.text = "" + plantIndex;
            PlantsMenu.SetActive(true);
            plantInfoItems.SetActive(true);
            plantTitle.enabled = true;
        }
    }

    public void OnRightArrowClick()
    {
        if (plantIndex != allPlots.Length - 1)
            plantIndex += 1;

        if (allPlots[plantIndex] != null)
        {
            plantTitle.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantName;
            plantInfo.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantInfo;
            plantImage.sprite = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantImage;
            potIndex.text = plantIndex.ToString();
        }
    }

    public void OnLeftArrowClick()
    {
        if (plantIndex != 0)
            plantIndex -= 1;
        if (allPlots[plantIndex] != null)
        {
            plantTitle.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantName;
            plantInfo.text = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantInfo;
            plantImage.sprite = allPlots[plantIndex].transform.gameObject.
                GetComponent<PlantTimer>().plantImage;
            potIndex.text = plantIndex.ToString();
        }
    }

    public void OnAnimalsClick()
    {
        AnimalsMenu.enabled = true;
    }

    // Close Menus
    public void CloseMenuClick()
    {
        shopMenu.enabled = false;
        LevelMenu.SetActive(false);
        PlantsMenu.SetActive(false);
        AnimalsMenu.enabled = false;

        // Disable all shop buttons

        menuTabs.SetActive(false);
        shopMenu.transform.Find("PlotsButtons").gameObject.SetActive(false);
        shopMenu.transform.Find("PlantsButtons").gameObject.SetActive(false);
        shopMenu.transform.Find("DecorButtons").gameObject.SetActive(false);
        shopMenu.transform.Find("AnimalsButtons").gameObject.SetActive(false);

        LevelMenu.transform.Find("LevelSelectMenu").gameObject.SetActive(false);

        PlantsMenu.transform.Find("PlantInfoMenu").gameObject.SetActive(false);
        PlantsMenu.transform.Find("TriviaItems").gameObject.SetActive(false);
        PlantsMenu.transform.Find("RightArrow").gameObject.SetActive(false);
        PlantsMenu.transform.Find("LeftArrow").gameObject.SetActive(false);
    }
}
